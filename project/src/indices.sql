CREATE INDEX products_name ON products(name);
CREATE INDEX products_category_id ON products(category_id);
CREATE INDEX orders_client_inn ON orders(client_inn);
CREATE INDEX clients_company_name ON clients(company_name);
