-- Проверяет, является ли значение val корректным:
CREATE OR REPLACE FUNCTION check_val_is_valid(val VARCHAR, val_type VARCHAR, min_val NUMBER, 
    max_val NUMBER, min_step NUMBER) RETURN INTEGER
IS
    val_ NUMBER;
BEGIN
    IF (val_type = 'str') THEN
        RETURN 0;
    END IF;

    val_ := to_number(val);
    IF (val_type = 'int') THEN
        IF (FLOOR(val_) != val_) THEN 
            raise_application_error(-20002, 'val is not an integer');
        END IF;
    END IF;

    IF (MOD(val_ - min_val, min_step) != 0) THEN
        raise_application_error(-20003, 'mod(val - min_val, min_step) != 0');
    ELSIF (val_ < min_val) THEN
        raise_application_error(-20004, 'val < min_val');
    ELSIF (val_ > max_val) THEN
        raise_application_error(-20005, 'val > max_val');
    END IF;
    RETURN 0;
END;
/
