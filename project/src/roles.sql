-- Маркетолог:
CREATE ROLE marketer;
GRANT SELECT, UPDATE, INSERT, DELETE ON product_categories TO marketer;
GRANT SELECT, UPDATE, INSERT, DELETE ON product_attributes TO marketer;
GRANT SELECT, UPDATE, INSERT, DELETE ON categories_attributes TO marketer;

-- Менеджер заказов:
CREATE ROLE order_manager;
GRANT SELECT ON products TO order_manager;
GRANT SELECT ON product_categories TO order_manager;
GRANT SELECT ON product_attributes TO order_manager;
GRANT SELECT, UPDATE, INSERT, DELETE ON clients TO order_manager;
GRANT SELECT, UPDATE, INSERT, DELETE ON orders TO order_manager;
GRANT SELECT ON categories_attributes TO order_manager;
GRANT SELECT ON products_categories_attributes TO order_manager;
GRANT SELECT, UPDATE, INSERT, DELETE ON orders_products TO order_manager;

-- Менеджер товаров:
CREATE ROLE product_manager;
GRANT SELECT, UPDATE, INSERT, DELETE ON products TO product_manager;
GRANT SELECT ON product_categories TO product_manager;
GRANT SELECT ON product_attributes TO product_manager;
GRANT SELECT ON categories_attributes TO product_manager;
GRANT SELECT, UPDATE ON products_categories_attributes TO product_manager;
