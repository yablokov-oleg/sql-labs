-- Для product_categories:
CREATE SEQUENCE product_categories_seq INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- Для product_attributes:
CREATE SEQUENCE product_attributes_seq INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- Для orders:
CREATE SEQUENCE orders_seq INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;

-- Для product_attributes:
CREATE SEQUENCE product_attributes_seq INCREMENT BY 1 START WITH 1 NOMAXVALUE NOCYCLE;
