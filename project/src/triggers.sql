-- Автоматическое определение цены при добавлении в orders_products:
CREATE OR REPLACE TRIGGER orders_products_insert
BEFORE INSERT ON orders_products
FOR EACH ROW
BEGIN
    IF (:new.price IS NULL) THEN
        SELECT price INTO :new.price FROM products WHERE vendor_code = :new.vendor_code;
    END IF;
END;
/

-- -- Автоматическое добавление логина текущего пользователя в поле ulogin таблицы orders:
-- CREATE OR REPLACE TRIGGER orders_auto_set_manager_ulogin
-- BEFORE INSERT ON orders
-- FOR EACH ROW
-- BEGIN
--     :new.manager_ulogin := user;
-- END;
-- /

-- Реализация логики завершения (отката завершения) заказа:
CREATE OR REPLACE TRIGGER orders_completion
BEFORE INSERT OR UPDATE ON orders
FOR EACH ROW
BEGIN
    IF (:old.date_performed IS NULL) AND (:new.date_performed IS NOT NULL) THEN
        -- пытаемся завершить заказ
        FOR el1 IN (SELECT vendor_code, amount FROM orders_products WHERE order_id = :new.id) LOOP
            FOR el2 IN (SELECT amount FROM products WHERE vendor_code = el1.vendor_code) LOOP
                IF (el2.amount - el1.amount >= 0) THEN 
                    UPDATE products
                        SET amount = el2.amount - el1.amount
                        WHERE vendor_code = el1.vendor_code;
                ELSE
                    raise_application_error(-20001, 'The order cannot be completed because 
                                                     the amount of products is not enough');
                END IF;
            END LOOP;
        END LOOP;
    ELSIF (:old.date_performed IS NOT NULL) AND (:new.date_performed IS NULL) THEN
        -- аннулируем заказ
        FOR el1 IN (SELECT vendor_code, amount FROM orders_products WHERE order_id = :new.id) LOOP
            FOR el2 IN (SELECT amount FROM products WHERE vendor_code = el1.vendor_code) LOOP
                UPDATE products
                    SET amount = el2.amount + el1.amount
                    WHERE vendor_code = el1.vendor_code;
            END LOOP;
        END LOOP;
    END IF;
END;
/

-- Обновление products_categories_attributes при добавлении строк в products:
CREATE OR REPLACE TRIGGER products_insert
AFTER INSERT ON products
FOR EACH ROW
BEGIN
    FOR attr IN (SELECT * FROM categories_attributes WHERE category_id = :new.category_id) LOOP
        INSERT INTO products_categories_attributes VALUES(
            :new.vendor_code,
            :new.category_id,
            attr.attribute_id,
            attr.default_val
        );
    END LOOP;
END;
/

-- Обновление products_categories_attributes при изменении строк в products:
CREATE OR REPLACE TRIGGER products_update
AFTER UPDATE ON products
FOR EACH ROW
BEGIN
    IF (:old.category_id != :new.category_id) THEN
        -- удаляем старые записи из products_categories_attributes
        DELETE FROM products_categories_attributes WHERE(
            vendor_code = :old.vendor_code AND
            category_id = :old.category_id
        );

        -- добавляем новые записи в products_categories_attributes
        FOR attr IN (SELECT * FROM categories_attributes WHERE category_id = :new.category_id) LOOP
            INSERT INTO products_categories_attributes VALUES(
                :new.vendor_code,
                :new.category_id,
                attr.attribute_id,
                attr.default_val
            );
        END LOOP;
    END IF;
END;
/

-- Проверка на то, что возможно поменять val_type в product_attributes без нарушения согласованности: 
CREATE OR REPLACE TRIGGER product_attributes_check_val_when_new_type
BEFORE UPDATE ON product_attributes
FOR EACH ROW
DECLARE
   ret INTEGER;
BEGIN
    IF (:new.val_type != :old.val_type) THEN
        FOR el IN (SELECT * FROM categories_attributes 
            WHERE attribute_id = :new.id)
        LOOP
            FOR el2 IN (SELECT * FROM products_categories_attributes 
                WHERE category_id = el.category_id AND attribute_id = el.attribute_id)
            LOOP
                -- кинет исключение, если что-то пойдёт не так
                ret := check_val_is_valid(el2.val, :new.val_type, el.min_val, el.max_val, el.min_step);
            END LOOP;
        END LOOP;
    END IF;
END;
/

-- Проверка, что min_val, max_val и min_step в categories_attributes = NULL, когда val_type = 'str':
CREATE OR REPLACE TRIGGER categories_attributes_min_max_val_step_are_null_when_val_type_is_str
BEFORE INSERT OR UPDATE ON categories_attributes
FOR EACH ROW
DECLARE
   val_type CHAR(3);
BEGIN
    EXECUTE IMMEDIATE 'SELECT val_type FROM product_attributes WHERE id = ' || :new.attribute_id
        INTO val_type;
    
    IF (val_type = 'str') THEN
        IF (:new.min_val IS NOT NULL) OR (:new.max_val IS NOT NULL) OR (:new.min_step IS NOT NULL) THEN
            raise_application_error(-20006, 'min_val, max_val and min_step in categories_attributes 
                                             must be NULL when val_type is str');
        END IF;
    ELSIF (val_type = 'int' OR val_type = 'num') THEN
        IF (:new.min_val IS NULL) OR (:new.max_val IS NULL) OR (:new.min_step IS NULL) THEN
            raise_application_error(-20007, 'min_val, max_val and min_step in categories_attributes 
                                             must be NOT NULL when val_type is int or num');
        END IF;
    END IF;
END;
/

-- Обновление products_categories_attributes при добавлении строк в categories_attributes:
CREATE OR REPLACE TRIGGER categories_attributes_insert
AFTER INSERT ON categories_attributes
FOR EACH ROW
DECLARE
   ret          INTEGER;
   val_type     CHAR(3);
BEGIN
    EXECUTE IMMEDIATE 'SELECT val_type FROM product_attributes WHERE id = ' || :new.attribute_id
        INTO val_type;
    
    -- проверяем на корректность val
    ret := check_val_is_valid(:new.default_val, val_type, :new.min_val, 
                              :new.max_val, :new.min_step);

    -- обеспечиваем согласованность с products_categories_attributes
    FOR prod IN (SELECT * FROM products WHERE category_id = :new.category_id) LOOP
        INSERT INTO products_categories_attributes VALUES(
            prod.vendor_code,
            :new.category_id,
            :new.attribute_id,
            :new.default_val
        );
    END LOOP;
END;
/

-- Обновление products_categories_attributes при изменении строк в categories_attributes:
CREATE OR REPLACE TRIGGER categories_attributes_update
AFTER UPDATE ON categories_attributes
FOR EACH ROW
DECLARE
   ret          INTEGER;
   val_type     CHAR(3);
BEGIN
    EXECUTE IMMEDIATE 'SELECT val_type FROM product_attributes WHERE id = ' || :new.attribute_id
        INTO val_type;
    
    -- проверяем на корректность val
    ret := check_val_is_valid(:new.default_val, val_type, :new.min_val, 
                              :new.max_val, :new.min_step);
    
    -- обеспечиваем согласованность с products_categories_attributes
    IF (:old.category_id != :new.category_id) THEN
        -- удаляем старые записи из products_categories_attributes
        DELETE FROM products_categories_attributes WHERE(
            category_id = :old.category_id AND
            attribute_id = :old.attribute_id
        );

        -- добавляем новые записи в products_categories_attributes
        FOR prod IN (SELECT * FROM products WHERE category_id = :new.category_id) LOOP
            INSERT INTO products_categories_attributes VALUES(
                prod.vendor_code,
                :new.category_id,
                :new.attribute_id,
                :new.default_val
            );
        END LOOP;
    ELSIF (:old.attribute_id != :new.attribute_id) THEN
        -- добавляем записи с новыми attribute_id
        FOR el IN (SELECT * FROM products_categories_attributes 
                   WHERE category_id = :old.category_id AND attribute_id = :old.attribute_id)
        LOOP
            INSERT INTO products_categories_attributes 
                VALUES(el.vendor_code, el.category_id, :new.attribute_id, el.val);
        END LOOP;

        -- удаляем записи со старым attribute_id
        DELETE FROM products_categories_attributes
            WHERE category_id = :old.category_id AND attribute_id = :old.attribute_id;
    ELSE
        -- проверяем на корректность val
        FOR attr IN (SELECT val FROM products_categories_attributes 
                     WHERE category_id = :old.category_id AND attribute_id = :old.attribute_id) 
        LOOP
            ret := check_val_is_valid(attr.val, val_type, :new.min_val, 
                                      :new.max_val, :new.min_step);
        END LOOP;
    END IF;
END;
/

-- Проверяет, является ли значение val корректным при обновлении products_categories_attributes:
CREATE OR REPLACE TRIGGER products_categories_attributes_check_val
BEFORE UPDATE ON products_categories_attributes
FOR EACH ROW
DECLARE
   ret INTEGER;
BEGIN
    FOR attr IN (SELECT val_type FROM product_attributes WHERE id = :new.attribute_id) LOOP
        FOR el IN (SELECT * FROM categories_attributes 
            WHERE category_id = :new.category_id AND attribute_id = :new.attribute_id)
        LOOP
            -- кинет исключение, если что-то пойдёт не так
            ret := check_val_is_valid(:new.val, attr.val_type, el.min_val, el.max_val, el.min_step);
        END LOOP;
    END LOOP;
END;
/

-- Запрещение изменения внешних ключей в products_categories_attributes:
CREATE OR REPLACE TRIGGER products_categories_attributes_prohibit_changing_FK
BEFORE UPDATE ON products_categories_attributes
FOR EACH ROW
BEGIN
    IF (
        (:old.vendor_code != :new.vendor_code) OR 
        (:old.category_id != :new.category_id) OR 
        (:old.attribute_id != :new.attribute_id)
    ) THEN
        raise_application_error(-20000, 'Only val attribute can be changed in this table');
    END IF;
END;
/
