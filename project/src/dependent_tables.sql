-- Категории - характеристики товаров:
CREATE TABLE categories_attributes(
    category_id                 INTEGER NOT NULL,
    attribute_id                INTEGER NOT NULL,
    min_val                     NUMBER,
    max_val                     NUMBER,
    default_val                 VARCHAR(32) NOT NULL,
    min_step                    NUMBER,

    CONSTRAINT categories_attributes_category_fk
        FOREIGN KEY(category_id)
        REFERENCES product_categories(id),
    CONSTRAINT categories_attributes_attribute_fk
        FOREIGN KEY(attribute_id)
        REFERENCES product_attributes(id),
    CONSTRAINT categories_attributes_composite_pk 
        PRIMARY KEY(category_id, attribute_id)
);

-- Товары - категории товаров - характеристики товаров:
CREATE TABLE products_categories_attributes(
    vendor_code                 CHAR(8) NOT NULL,
    category_id                 INTEGER NOT NULL,
    attribute_id                INTEGER NOT NULL,
    val                         VARCHAR(32) NOT NULL,
    
    CONSTRAINT products_categories_attributes_vendor_code_fk
        FOREIGN KEY(vendor_code)
        REFERENCES products(vendor_code)
        ON DELETE CASCADE,
    CONSTRAINT products_categories_attributes_composite_fk
        FOREIGN KEY(category_id, attribute_id)
        REFERENCES categories_attributes(category_id, attribute_id)
        ON DELETE CASCADE,
    CONSTRAINT products_categories_attributes_composite_pk
        PRIMARY KEY(category_id, vendor_code, attribute_id)
);

-- Заказы - товары:
CREATE TABLE orders_products(
    order_id                INTEGER NOT NULL,
    vendor_code             CHAR(8) NOT NULL,
    price                   NUMBER DEFAULT NULL NOT NULL,
    amount                  INTEGER DEFAULT 1 NOT NULL,

    CONSTRAINT orders_products_vendor_code_fk
        FOREIGN KEY(vendor_code)
        REFERENCES products(vendor_code),
    CONSTRAINT orders_products_category_fk
        FOREIGN KEY(order_id)
        REFERENCES orders(id)
        ON DELETE CASCADE,
    CONSTRAINT orders_products_composite_pk 
        PRIMARY KEY(order_id, vendor_code),
    CHECK(price > 0),
    CHECK(amount > 0)
);
