-- Категории товаров:
CREATE TABLE product_categories(
    id                          INTEGER PRIMARY KEY,
    name                        VARCHAR(64) NOT NULL UNIQUE
);

-- Товары:
CREATE TABLE products(
    vendor_code                 CHAR(8) PRIMARY KEY,
    model                       VARCHAR(32) NOT NULL,
    series                      VARCHAR(32),
    price                       NUMBER NOT NULL,
    amount                      INTEGER DEFAULT 0 NOT NULL,
    category_id                 INTEGER NOT NULL,

    CONSTRAINT products_category_id_fk
        FOREIGN KEY(category_id)
        REFERENCES product_categories(id),
    CONSTRAINT products_model_series_unique
        UNIQUE(model, series),
    CHECK(price > 0),
    CHECK(amount >= 0)
);

-- Характеристики товаров:
CREATE TABLE product_attributes(
    id                          INTEGER PRIMARY KEY,
    name                        VARCHAR(64) NOT NULL UNIQUE,
    val_type                    CHAR(3) NOT NULL,

    CHECK(val_type = 'int' OR val_type = 'num' OR val_type = 'str')
);

-- Клиенты:
CREATE TABLE clients(
    inn                         CHAR(12) PRIMARY KEY,
    company_name                VARCHAR(64) NOT NULL UNIQUE
);

-- Заказы:
CREATE TABLE orders(
    id                          INTEGER PRIMARY KEY,
    client_inn                  CHAR(12) NOT NULL,
    date_created                DATE DEFAULT SYSDATE NOT NULL,
    date_performed              DATE,

    CONSTRAINT client_inn_fk
        FOREIGN KEY(client_inn)
        REFERENCES clients(inn),
    CHECK(date_created <= date_performed OR date_performed IS NULL)
);
