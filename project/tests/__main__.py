import unittest

from base_tests import *

import utility
import os
import sys
import getpass


if __name__ == '__main__':
    use_args = False
    if use_args:
        print('This is a utility for testing the database.\n'
              'Note that to use it you must have a running database instance with schemas created.\n'
              'The utility assumes there are no data in tables and will not commit the changes done while testing.')
        host = input(
            'Host to connect to database (leave blank to use localhost): ')
        login = input('Login to connect to database: ')
        password = getpass.getpass('Password to connect to database: ')

    if not host:
        host = '127.0.0.1'

    utility.connect_to_database.host = host
    utility.connect_to_database.login = login
    utility.connect_to_database.password = password
    
    unittest.main()
