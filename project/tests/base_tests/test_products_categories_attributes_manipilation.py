from utility.connect_to_database import connect_to_database

import unittest
import cx_Oracle


class TestProductsCategoriesAttributesManipulation(unittest.TestCase):
    def setUp(self):
        self.m_Connection = connect_to_database()
        self.m_Cursor = self.m_Connection.cursor()
        self.m_Cursor.execute(
            'INSERT INTO product_categories VALUES(1, \'category1\')')
        self.m_Cursor.execute(
            'INSERT INTO product_categories VALUES(2, \'category2\')')
        self.m_Cursor.execute(
            'INSERT INTO products VALUES(\'00000001\', \'model1\', \'series1\', 9.99, 10, 1)')
        self.m_Cursor.execute(
            'INSERT INTO product_attributes VALUES(1, \'attribute1\', \'int\')')
        self.m_Cursor.execute(
            'INSERT INTO categories_attributes VALUES(1, 1, 2, 10, \'4\', 2)')

    def tearDown(self):
        self.m_Connection.close()

    def test_only_val_can_be_updated_in_products_categories_attributes(self):
        with self.assertRaises(cx_Oracle.DatabaseError) as er:
            self.m_Cursor.execute(
                'UPDATE products_categories_attributes SET category_id = 2')
        self.assertEqual(er.exception.args[0].code, 20000)

    def test_val_is_valid_when_updated_in_products_categories_attributes(self):
        with self.assertRaises(cx_Oracle.DatabaseError) as er:
            self.m_Cursor.execute(
                'UPDATE products_categories_attributes SET val = \'4.4\'')
        self.assertEqual(er.exception.args[0].code, 20002)

        with self.assertRaises(cx_Oracle.DatabaseError) as er:
            self.m_Cursor.execute(
                'UPDATE products_categories_attributes SET val = \'3\'')
        self.assertEqual(er.exception.args[0].code, 20003)

        with self.assertRaises(cx_Oracle.DatabaseError) as er:
            self.m_Cursor.execute(
                'UPDATE products_categories_attributes SET val = \'0\'')
        self.assertEqual(er.exception.args[0].code, 20004)
        
        with self.assertRaises(cx_Oracle.DatabaseError) as er:
            self.m_Cursor.execute(
                'UPDATE products_categories_attributes SET val = \'12\'')
        self.assertEqual(er.exception.args[0].code, 20005)
