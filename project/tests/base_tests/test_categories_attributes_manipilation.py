from utility.connect_to_database import connect_to_database

import unittest
import cx_Oracle


class TestCategoriesAttributesManipulation(unittest.TestCase):
    def setUp(self):
        self.m_Connection = connect_to_database()
        self.m_Cursor = self.m_Connection.cursor()

        self.m_Cursor.execute(
            'INSERT INTO product_categories VALUES(1, \'category1\')')
        self.m_Cursor.execute(
            'INSERT INTO product_categories VALUES(2, \'category2\')')
        self.m_Cursor.execute(
            'INSERT INTO products VALUES(\'00000001\', \'model1\', \'series1\', 9.99, 10, 1)')
        self.m_Cursor.execute(
            'INSERT INTO product_attributes VALUES(1, \'attribute1\', \'int\')')
        self.m_Cursor.execute(
            'INSERT INTO product_attributes VALUES(2, \'attribute2\', \'num\')')

    def tearDown(self):
        self.m_Connection.close()

    def test_products_categories_attributes_is_updated_after_insert_on_categories_attributes(self):
        self.m_Cursor.execute(
            'INSERT INTO categories_attributes VALUES(1, 1, 0, 10, \'5\', 1)')
        self.m_Cursor.execute(
            'SELECT * FROM products_categories_attributes')

        res = self.m_Cursor.fetchall()
        self.assertEqual(len(res), 1)
        for el in res:
            self.assertEqual(el[0], '00000001')
            self.assertEqual(el[1], 1)
            self.assertEqual(el[2], 1)
            self.assertEqual(el[3], '5')

    def test_products_categories_attributes_is_updated_after_update_on_categories_attributes(self):
        self.m_Cursor.execute(
            'INSERT INTO categories_attributes VALUES(1, 1, 0, 10, \'5\', 1)')
        self.m_Cursor.execute(
            'UPDATE categories_attributes SET attribute_id = 2')
        self.m_Cursor.execute(
            'SELECT * FROM products_categories_attributes')
        
        res = self.m_Cursor.fetchall()
        self.assertEqual(len(res), 1)
        for el in res:
            self.assertEqual(el[2], 2)

    def test_products_categories_attributes_is_updated_after_delete_on_categories_attributes(self):
        self.m_Cursor.execute(
            'INSERT INTO categories_attributes VALUES(1, 1, 0, 10, \'5\', 1)')
        self.m_Cursor.execute(
            'DELETE FROM categories_attributes')
        self.m_Cursor.execute(
            'SELECT * FROM products_categories_attributes')

        res = self.m_Cursor.fetchall()
        self.assertEqual(len(res), 0)

    def test_val_is_valid_when_insert_or_update_categories_attributes(self):
        with self.assertRaises(cx_Oracle.DatabaseError) as er:
            self.m_Cursor.execute(
                'INSERT INTO categories_attributes VALUES(1, 1, 1, 10, \'4.4\', 1)')
        self.assertEqual(er.exception.args[0].code, 20002)

        with self.assertRaises(cx_Oracle.DatabaseError) as er:
            self.m_Cursor.execute(
                'INSERT INTO categories_attributes VALUES(1, 1, 1, 10, \'3\', 3)')
        self.assertEqual(er.exception.args[0].code, 20003)

        with self.assertRaises(cx_Oracle.DatabaseError) as er:
            self.m_Cursor.execute(
                'INSERT INTO categories_attributes VALUES(1, 1, 1, 10, \'0\', 1)')
        self.assertEqual(er.exception.args[0].code, 20004)

        with self.assertRaises(cx_Oracle.DatabaseError) as er:
            self.m_Cursor.execute(
                'INSERT INTO categories_attributes VALUES(1, 1, 1, 10, \'12\', 1)')
        self.assertEqual(er.exception.args[0].code, 20005)
