from utility.connect_to_database import connect_to_database

import unittest
import cx_Oracle


class TestProductsManipulation(unittest.TestCase):
    def setUp(self):
        self.m_Connection = connect_to_database()
        self.m_Cursor = self.m_Connection.cursor()

        self.m_Cursor.execute(
            'INSERT INTO product_categories VALUES(1, \'category1\')')
        self.m_Cursor.execute(
            'INSERT INTO product_attributes VALUES(1, \'attribute1\', \'num\')')
        self.m_Cursor.execute(
            'INSERT INTO categories_attributes VALUES(1, 1, 0, 10, \'5\', 1)')

    def tearDown(self):
        self.m_Connection.close()

    def test_products_categories_attributes_is_updated_after_insert_on_products(self):
        self.m_Cursor.execute(
            'INSERT INTO products VALUES(\'00000001\', \'model1\', \'series1\', 9.99, 10, 1)')
        self.m_Cursor.execute(
            'SELECT * FROM products_categories_attributes')
        
        res = self.m_Cursor.fetchall()
        self.assertEqual(len(res), 1)
        for el in res:
            self.assertEqual(el[0], '00000001')
            self.assertEqual(el[1], 1)
            self.assertEqual(el[2], 1)
            self.assertEqual(el[3], '5')
