from utility.connect_to_database import connect_to_database

import unittest
import cx_Oracle


class TestCorrectOrdersCompletion(unittest.TestCase):
    def setUp(self):
        self.m_Connection = connect_to_database()
        self.m_Cursor = self.m_Connection.cursor()
        self.m_Cursor.execute(
            'INSERT INTO clients VALUES(\'000000000001\', \'company1\')')
        self.m_Cursor.execute(
            'INSERT INTO product_categories VALUES(1, \'category1\')')
        self.m_Cursor.execute(
            'INSERT INTO products VALUES(\'00000001\', \'model1\', \'series1\', 9.99, 10, 1)')
        self.m_Cursor.execute(
            'INSERT INTO products VALUES(\'00000002\', \'model2\', \'series2\', 9.99, 5, 1)')
        self.m_Cursor.execute(
            'INSERT INTO orders VALUES(1, \'000000000001\', SYSDATE, NULL)')
        self.m_Cursor.execute(
            'INSERT INTO orders_products VALUES(1, \'00000001\', 9.99, 8)')
        self.m_Cursor.execute(
            'INSERT INTO orders_products VALUES(1, \'00000002\', 9.99, 8)')

    def tearDown(self):
        self.m_Connection.close()

    def test_order_with_enough_products_can_be_completed(self):
        self.m_Cursor.execute(
            'UPDATE orders_products SET amount = 2 WHERE vendor_code = \'00000002\'')
        self.m_Cursor.execute(
            'UPDATE orders SET date_performed = SYSDATE WHERE id = 1')
        self.m_Cursor.execute(
            'SELECT vendor_code, amount FROM products')
        
        res = self.m_Cursor.fetchall()
        self.assertEqual(len(res), 2)
        for el in res:
            if el[0] == '00000001':
                self.assertEqual(el[1], 2)
            elif el[0] == '00000002':
                self.assertEqual(el[1], 3)
            else:
                raise ValueError('What the heck?')

    def test_order_with_not_enough_products_cannot_be_completed(self):
        with self.assertRaises(cx_Oracle.DatabaseError) as er:
            self.m_Cursor.execute(
                'UPDATE orders SET date_performed = SYSDATE WHERE id = 1')
        self.assertEqual(er.exception.args[0].code, 20001)

    def test_completed_order_can_be_correctly_set_uncompleted(self):
        self.m_Cursor.execute(
            'DELETE FROM orders_products WHERE vendor_code = \'00000002\'')
        self.m_Cursor.execute(
            'UPDATE orders SET date_performed = SYSDATE WHERE id = 1')
        self.m_Cursor.execute(
            'UPDATE orders SET date_performed = NULL WHERE id = 1')
        self.m_Cursor.execute(
            'SELECT vendor_code, amount FROM products')
        
        res = self.m_Cursor.fetchall()
        self.assertEqual(len(res), 2)
        for el in res:
            if el[0] == '00000001':
                self.assertEqual(el[1], 10)
            elif el[0] == '00000002':
                pass
            else:
                raise ValueError('What the heck?')
