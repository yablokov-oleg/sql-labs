INSERT INTO science_sections VALUES(
    1, 'Математика'
);
INSERT INTO science_sections VALUES(
    2, 'Физика'
);
INSERT INTO science_sections VALUES(
    3, 'Информатика'
);


INSERT INTO scientific_areas VALUES(
    '1', 'Физика конденсированного состояния', 2
);
INSERT INTO scientific_areas VALUES(
    '2', 'Уравнения в частных производных', 1
);
INSERT INTO scientific_areas VALUES(
    '3', 'Искусственный интелект', 3
);
INSERT INTO scientific_areas VALUES(
    '4', 'Теория струн', 2
);
INSERT INTO scientific_areas VALUES(
    '5', 'Телекоммуникационные системы и сети', 3
);
INSERT INTO scientific_areas VALUES(
    '6', 'Динамика сплошных сред', 2
);


INSERT INTO authors VALUES(
    'Иван Тапочкин', '24-DEC-1993', 'м', '0000123456', '24-DEC-1953'
);
INSERT INTO authors VALUES(
    'Мария Абрикосова', '05-APR-1991', 'ж', '9832654321', '24-DEC-1963'
);
INSERT INTO authors VALUES(
    'Андрей Плюшкин', '11-SEP-1992', 'м', '7781214365', '24-DEC-1953'
);
INSERT INTO authors VALUES(
    'Виниамин Московский', '11-OCT-1993', 'м', '6401314253', '24-DEC-1953'
);
INSERT INTO authors VALUES(
    'Александр Морозов', '03-SEP-1995', 'м', '0391214153', '24-DEC-1943'
);
INSERT INTO authors VALUES(
    'Елизавета Агрономова', '25-AUG-1998', 'ж', '0010416231', '24-DEC-1943'
);


INSERT INTO dissertations VALUES(
    1, '5', '0391214153', 'Диссертация1', 'кандидатская', 
    '04-APR-2017', 'орг1', '11-APR-2017', '1'
);
INSERT INTO dissertations VALUES(
    2, '5', '0010416231', 'Диссертация2', 'кандидатская', 
    '22-JUN-2018', 'орг2', NULL, '2'
);

INSERT INTO dissertations VALUES(
    3, '3', '7781214365', 'Диссертация3', 'докторская', 
    '04-APR-2018', 'орг1', '11-APR-2018', '3'
);
INSERT INTO dissertations VALUES(
    4, '1', '7781214365', 'Диссертация4', 'кандидатская', 
    '22-JUN-2018', 'орг2', '29-JUN-2018', '4'
);

INSERT INTO dissertations VALUES(
    5, '2', '0000123456', 'Диссертация5', 'докторская', 
    '14-MAY-2016', 'орг3', '26-JUN-2018', '5'
);
INSERT INTO dissertations VALUES(
    6, '2', '0000123456', 'Диссертация6', 'кандидатская', 
    '14-MAY-2016', 'орг3', '19-JUN-2016', '6'
);

INSERT INTO dissertations VALUES(
    7, '2', '6401314253', 'Диссертация7', 'кандидатская', 
    '19-NOV-2018', 'орг3', '01-dec-2018', '7'
);
INSERT INTO dissertations VALUES(
    8, '1', '6401314253', 'Диссертация8', 'докторская', 
    '19-NOV-2018', 'орг3', NULL, '8'
);
