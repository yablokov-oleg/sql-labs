-- Вспомогательные таблицы и представления:
CREATE TABLE members_of_RAS(
    full_name       VARCHAR(100) NOT NULL,
    birth           DATE NOT NULL,
    sex             CHAR(2) NOT NULL,
    passport_id     CHAR(10) PRIMARY KEY,

    CHECK(sex = 'м' OR sex = 'ж')
);

CREATE VIEW organisations_info AS
SELECT dis.organization org_name, dis.scientific_area area_id
    FROM dissertations dis
    WHERE date_acceptance IS NOT NULL;

CREATE VIEW math_areas AS
SELECT areas.id area_id
    FROM scientific_areas areas
    WHERE areas.science_section_cipher = 3;
--


-- Селекция:
SELECT *
    FROM authors
    WHERE sex = 'ж';


-- Проекция:
SELECT DISTINCT full_name, passport_id
    FROM authors;


-- Декартово произведение:
SELECT *
    FROM authors, members_of_RAS;


-- Объединение:
SELECT *
    FROM authors
UNION
SELECT *
    FROM members_of_RAS;


-- Разность:
SELECT DISTINCT *
    FROM students
    WHERE NOT EXISTS (
        SELECT DISTINCT *
            FROM employees
        WHERE
            students.name = employees.name AND
            students.surname = employees.surname AND
            students.middle_name = employees.middle_name AND
            students.birthday = employees.birthday AND
            students.address = employees.address
    );


-- Пересечение:
SELECT DISTINCT *
    FROM students
    JOIN employees
    ON
        students.name = employees.name AND
        students.surname = employees.surname AND
        students.middle_name = employees.middle_name AND
        students.birthday = employees.birthday AND
        students.address = employees.address;


-- Соединение:
SELECT *
    FROM science_sections sections, scientific_areas areas
    WHERE sections.cipher = areas.science_section_cipher;


-- Деление (ищем организации, сотрудники которых защитили дипломы по ВСЕМ научным направлениям из math_areas):
SELECT org_name
    FROM organisations_info
MINUS
SELECT org_name
FROM (
    SELECT *
    FROM (
        SELECT org_name
            FROM organisations_info
    ), math_areas
    MINUS
    SELECT *
        FROM organisations_info
);
