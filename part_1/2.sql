-- Диссертации по научному направлению «Телекоммуникационные системы и сети»:
SELECT dis.*
    FROM dissertations dis
        JOIN scientific_areas areas
        ON dis.scientific_area = areas.id
    WHERE areas.name = 'Телекоммуникационные системы и сети'
    ORDER BY dis.name;

-- Авторы, у которых после даты защиты прошло больше месяца, но диссертация ещё не утверждена:
SELECT authors.full_name
    FROM authors 
        JOIN dissertations dis
        ON authors.passport_id = dis.author_passport_id
    WHERE
        MONTHS_BETWEEN(SYSDATE, dis.date_defence) > 1 AND
        date_acceptance IS NULL
    ORDER BY authors.full_name;

-- Количество диссертаций по разделам науки, защищенных в текущем году:
SELECT sec.name, COUNT(dis.id)
    FROM science_sections sec, dissertations dis, scientific_areas areas
    WHERE
        dis.scientific_area = areas.id AND
        areas.science_section_cipher = sec.cipher AND
        date_acceptance IS NOT NULL AND
        EXTRACT(YEAR FROM dis.date_defence) = EXTRACT(YEAR FROM SYSDATE)
    GROUP BY sec.name
    ORDER BY sec.name;

-- Научные направления, по которым нет докторских диссертаций:
SELECT areas.name
    FROM scientific_areas areas
        LEFT JOIN dissertations dis 
        ON 
            areas.id = dis.scientific_area AND
            dis.type = 'докторская'
    WHERE dis.id IS NULL
    ORDER BY areas.name;

-- Авторы, которые защитили кандидатскую и докторскую диссертации по разным направлениям науки:
SELECT authors.full_name
    FROM ( 
        SELECT dis.author_passport_id
            FROM dissertations dis
            GROUP BY dis.author_passport_id
            HAVING
                COUNT(DISTINCT dis.scientific_area) > 1 AND
                COUNT(DISTINCT dis.type) > 1
    ) t1 
        JOIN authors 
        ON t1.author_passport_id = authors.passport_id
    ORDER BY authors.full_name;

-- Другой вариант предыдущего запроса:
SELECT authors.full_name
    FROM ( 
        SELECT dis.author_passport_id
            FROM dissertations dis
            GROUP BY dis.author_passport_id
            HAVING COUNT(DISTINCT dis.scientific_area) > 1
    ) t1 
        LEFT JOIN authors 
        ON t1.author_passport_id = authors.passport_id
    WHERE t1.author_passport_id IN (
        SELECT dis.author_passport_id
            FROM dissertations dis
            GROUP BY dis.author_passport_id
            HAVING COUNT(DISTINCT dis.type) > 1
    )
    ORDER BY authors.full_name;
-- Он будет медленнее, так как для каждого автора его паспортные данные будут сверяться со
-- Всей таблицей из второго подзапроса (в первом варианте запроса по dissertations всего
-- один проход, что быстрее)
