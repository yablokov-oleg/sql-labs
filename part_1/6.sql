-- Запросы были проверены в PostgreSQL

-- Вспомогательная таблица:
CREATE TABLE lab6(
    id      INTEGER PRIMARY KEY,
    val     VARCHAR(20)
);
INSERT INTO lab6 VALUES (
    1, 'val1'
); 
--


-- COMMIT:
INSERT INTO lab6 VALUES (           -- Запрос изолирован
    2, 'val2'
); 
CREATE TABLE lab6_new(              -- Запрос изолирован
    id      INTEGER PRIMARY KEY,
    val     VARCHAR(20)
);
DROP TABLE lab6;                    -- Запрос изолирован 
UPDATE lab6                         -- Запрос изолирован 
    SET val = 'updated val'
    WHERE id = 1;
TRUNCATE TABLE lab6;                -- Клиент зависает (происходит блокировка)


-- ROLLBACK и SAVEPOINT: обе команды не меняют изолированность при AUTOCOMMIT=OFF; 
-- при AUTOCOMMIT=ON эти команды не работают.
