-- Разделы науки:
CREATE TABLE science_sections(
    cipher      INTEGER PRIMARY KEY,
    name        VARCHAR(100) NOT NULL UNIQUE
);

-- Научные направления:
CREATE TABLE scientific_areas(
    id                      VARCHAR(100) PRIMARY KEY,
    name                    VARCHAR(100) NOT NULL UNIQUE,
    science_section_cipher  INTEGER NOT NULL,

    CONSTRAINT science_section_cipher 
        FOREIGN KEY (science_section_cipher) 
        REFERENCES science_sections(cipher)
);

-- Авторы:
CREATE TABLE authors(
    full_name               VARCHAR(100) NOT NULL,
    birth                   DATE NOT NULL,
    sex                     CHAR(2) NOT NULL,
    passport_id             CHAR(10) PRIMARY KEY,
    passport_date_receipt   DATE NOT NULL,

    CHECK(sex = 'м' OR sex = 'ж')
);

-- Диссертации:
CREATE TABLE dissertations(
    id                  NUMBER(5) PRIMARY KEY,
    scientific_area     VARCHAR(100),
    author_passport_id  CHAR(10) NOT NULL,
    name                VARCHAR(400) NOT NULL,
    type                VARCHAR(30),
    date_defence        DATE NOT NULL,
    organization        VARCHAR(120) NOT NULL,
    date_acceptance     DATE,
    diploma_id          VARCHAR(40) UNIQUE,

    CONSTRAINT scientific_area
        FOREIGN KEY (scientific_area)
        REFERENCES scientific_areas(id),
    CONSTRAINT author_passport_id
        FOREIGN KEY (author_passport_id)
        REFERENCES authors(passport_id),
    CHECK(type = 'кандидатская' OR type = 'докторская'),
    CHECK(date_defence <= date_acceptance)
);
