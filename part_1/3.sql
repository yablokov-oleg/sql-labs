-- 1. Представление "Количественные показатели": раздел науки – научное направление – 
-- количество кандидатов наук – количество докторов наук:
CREATE VIEW quantative_values AS
SELECT sections.name раздел_науки, areas.name научное_направление, t_kand.num количество_кандидатов_наук, t_doc.num количество_докторов_наук
    FROM scientific_areas areas
        LEFT JOIN science_sections sections
        ON areas.science_section_cipher = sections.cipher
        LEFT JOIN (
            SELECT dis.scientific_area area, COUNT(dis.author_passport_id) num
                FROM dissertations dis
                WHERE
                    dis.type = 'докторская' AND
                    dis.date_acceptance IS NOT NULL
                GROUP BY dis.scientific_area
        ) t_doc
        ON areas.id = t_doc.area
        LEFT JOIN (
            SELECT dis.scientific_area area, COUNT(dis.author_passport_id) num
                FROM dissertations dis
                WHERE
                    dis.type = 'кандидатская' AND
                    dis.date_acceptance IS NOT NULL AND
                    NOT EXISTS (
                        SELECT dis_.author_passport_id
                            FROM dissertations dis_
                            WHERE
                                dis_.type = 'докторская' AND
                                dis_.date_acceptance IS NOT NULL AND
                                dis.author_passport_id = dis_.author_passport_id AND
                                dis.scientific_area = dis_.scientific_area
                    )
                GROUP BY dis.scientific_area
        ) t_kand
        ON areas.id = t_kand.area
    ORDER BY areas.name;

-- Проверка на обновляемость:
INSERT INTO quantative_values VALUES(
    'Информатика', 'Телекоммуникационные системы и сети', 3, 0
);
UPDATE quantative_values
    SET научное_направление = 'Искусственный интелект'
    WHERE раздел_науки = 'Информатика';
-- В первом случае появилась ошибка:
-- "ORA-01779: cannot modify a column which maps to a non key-preserved table",
-- Т.к. чтобы восстановить логику построения представления, пришлось бы добавлять (изменять) 
-- строки в таблицах, но неизвестно, как именно надо это делать.
-- Во втором случае появилась ошибка:
-- "ORA-00001: unique constraint (SQL_HJUTLKNKBEMERFRSILYRKLBXQ.SYS_C008641031) violated ORA-06512: at "SYS.DBMS_SQL", line 1721",



-- 2. Представление "Доктора наук": автор – данные о его кандидатской диссертации – 
-- данные о его докторской диссертации:
CREATE VIEW phds AS
SELECT authors.full_name автор, k_dis.name кандидатская_диссертация, d_dis.name докторская_диссертация
    FROM authors
        LEFT JOIN dissertations k_dis
        ON
            authors.passport_id = k_dis.author_passport_id AND
            k_dis.type = 'кандидатская'
        LEFT JOIN dissertations d_dis
        ON 
            authors.passport_id = d_dis.author_passport_id AND
            d_dis.type = 'докторская'
        WHERE authors.passport_id IN (
            SELECT DISTINCT dis.author_passport_id
                FROM dissertations dis
                WHERE
                    dis.type = 'докторская' AND
                    dis.date_acceptance IS NOT NULL
                GROUP BY dis.author_passport_id
        )
    ORDER BY authors.full_name;

-- Проверка на обновляемость:
INSERT INTO phds VALUES(
    'Мария Абрикосова', 'Название 1', 'Название 2'
);
UPDATE phds
    SET автор = 'Андрей Плюшкин'
    WHERE автор = 'Иван Тапочкин';
-- В обоих случаях появилась ошибка:
-- "ORA-01779: cannot modify a column which maps to a non key-preserved table",
-- Т.к. чтобы восстановить логику построения представления, пришлось бы добавлять (изменять) 
-- строки в таблице dissertations, но неизвестно, как именно надо это делать.



-- 3. Представление "Научные работы" (соединение отношений "Авторы", 
-- "Диссертации" и "Научные направления"):
CREATE VIEW scientific_projects AS
SELECT authors.full_name автор, dis.name диссертация, areas.name научное_направление
    FROM dissertations dis
        LEFT JOIN scientific_areas areas
        ON dis.scientific_area = areas.id
        LEFT JOIN authors 
        ON authors.passport_id = dis.author_passport_id  
    ORDER BY authors.full_name;

-- Проверка на обновляемость:
INSERT INTO scientific_projects VALUES(
    'Александр Морозов', 'Новая диссертация', 'Искусственный интелект'
);
UPDATE scientific_projects
    SET диссертация = 'Диссертация по теории струн (1)'
    WHERE автор = 'Иван Тапочкин';
-- В первом случае появилась ошибка:
-- "ORA-01779: cannot modify a column which maps to a non key-preserved table",
-- Т.к. чтобы восстановить логику построения представления, пришлось бы добавлять (изменять) 
-- строки в таблице dissertations, но неизвестно, как именно надо это делать.
-- Во втором случае всё в порядке, т.к. для изменения поля "диссертация" СУБД необходимо
-- поменять это же поле в таблице dissertations, что является детерминированной операцией.
