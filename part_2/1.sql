-- 1:
-- Функция, преобразующая значение ФИО в фамилию с инициалами (например, "Иванов Иван
-- Сергеевич" в "Иванов И.С."). При невозможности преобразования функция возвращает строку
-- '#############'.
CREATE OR REPLACE FUNCTION get_reduced_fio(full_fio VARCHAR2) RETURN VARCHAR2
IS
    reduced_fio VARCHAR2(100) := '#############';
    delim1 NUMBER;
    delim2 NUMBER;
    first_name VARCHAR2(100);
    middle_name_abbr VARCHAR2(100);
    last_name_abbr VARCHAR2(100);
BEGIN
    delim1 := INSTR(full_fio, ' ', 1, 1);
    delim2 := INSTR(full_fio, ' ', delim1 + 1, 1);
    
    IF (delim1 != 0) AND (delim2 != 0) THEN
        first_name := SUBSTR(full_fio, 1, delim1);
        middle_name_abbr := SUBSTR(full_fio, delim1 + 1, 1);
        last_name_abbr := SUBSTR(full_fio, delim2 + 1, 1);
        
        IF (first_name IS NOT NULL) AND (middle_name_abbr IS NOT NULL) AND (last_name_abbr IS NOT NULL) THEN
            reduced_fio := CONCAT(first_name, CONCAT(middle_name_abbr, CONCAT('.', CONCAT(last_name_abbr, '.'))));
        END IF;
    END IF;
    
    RETURN reduced_fio;
END;
/

-- Проверка:
DECLARE 
   c VARCHAR(100);
BEGIN
    c := get_reduced_fio('Иванов Иван Сергеевич');
    dbms_output.put_line(c);
    c := get_reduced_fio('Иванов Иван');
    dbms_output.put_line(c);
    c := get_reduced_fio('Иванов  ');
    dbms_output.put_line(c);
    c := get_reduced_fio('  Иванов Иван Сергеевич');
    dbms_output.put_line(c);
    c := get_reduced_fio('Иванов  Иван Сергеевич');
    dbms_output.put_line(c);
END; 
/



-- 2:
-- Функция, выдающая полное название учетной степени по параметрам "Тип" и "Раздел науки".
-- Например, для типа "докторская" и раздела "Технические науки" функция должна вернуть
-- "доктор технических наук".
CREATE OR REPLACE FUNCTION get_full_academic_degree(type VARCHAR2, science_section VARCHAR2) RETURN VARCHAR2
IS
BEGIN
    IF (type = 'докторская') THEN
        IF (science_section = 'Математика') THEN
            RETURN 'доктор математических наук';
        ELSIF (science_section = 'Физика') THEN
            RETURN 'доктор физических наук';
        ELSIF (science_section = 'Информатика') THEN
            RETURN 'доктор информационных наук';
        END IF;
    ELSIF (type = 'кандидатская') THEN
        IF (science_section = 'Математика') THEN
            RETURN 'кандидат математических наук';
        ELSIF (science_section = 'Физика') THEN
            RETURN 'кандидат физических наук';
        ELSIF (science_section = 'Информатика') THEN
            RETURN 'кандидат информационных наук';
        END IF;
    END IF;
    RETURN '';
END;
/

-- Проверка:
DECLARE 
   c VARCHAR(100);
BEGIN
    c := get_full_academic_degree('докторская', 'Математика');
    dbms_output.put_line(c);
    c := get_full_academic_degree('докторская', 'Физика');
    dbms_output.put_line(c);
    c := get_full_academic_degree('sfd', 'f sdf');
    dbms_output.put_line(c);
    c := get_full_academic_degree('кандидатская', 'Математика');
    dbms_output.put_line(c);
    c := get_full_academic_degree('кандидатская', 'Физика');
    dbms_output.put_line(c);
END; 
/



-- 3:
-- Функция, выдающая возраст по двум датам: дате рождения и дате, на которую интересует
-- возраст. Если вторая дата не указана, то возраст на текущую дату. Вызывать функцию для двух
-- полей: дата рождения автора и дата защиты диссертации.
CREATE OR REPLACE FUNCTION get_age(birthday date, date2 date := sysdate) RETURN VARCHAR2
IS
    years NUMBER;
    months NUMBER;
    days NUMBER;
    months_total NUMBER;
    days_between_dates NUMBER := 0;
    ret VARCHAR2(100);
BEGIN
    days_between_dates := date2 - birthday;
    IF (days_between_dates < 0) THEN
        RETURN 'NULL';
    END IF;
    
    months_total := FLOOR(MONTHS_BETWEEN(date2, birthday));
    years := FLOOR(months_total / 12);
    months := months_total - years * 12;
    days := TO_NUMBER(TO_CHAR(date2, 'dd')) - TO_NUMBER(TO_CHAR(birthday, 'dd'));
    
    ret := CONCAT(years, ' years, ');
    ret := CONCAT(ret, CONCAT(months, ' months, '));
    ret := CONCAT(ret, CONCAT(days, ' days'));
    RETURN ret;
END;
/ 

-- Проверка:
DECLARE 
   c VARCHAR(100);
BEGIN
    c := get_age(TO_DATE('2012-06-05', 'YYYY-MM-DD'));
    dbms_output.put_line(c);
    c := get_age(TO_DATE('2012-06-04', 'YYYY-MM-DD'), TO_DATE('2012-08-03', 'YYYY-MM-DD'));
    dbms_output.put_line(c);
    c := get_age(TO_DATE('2020-06-04', 'YYYY-MM-DD'));
    dbms_output.put_line(c);
END; 
/
