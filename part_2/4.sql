-- Процедура, которая принимает в качестве параметра имя таблицы и выводит на
-- экран информацию обо всех таблицах, связанных с указанной таблицей по
-- внешнему ключу и принадлежащих пользователю, от имени которого запускается
-- эта процедура. Эта информация включает в себя имя таблицы, общее количество
-- записей и количество различных значений внешнего ключа.
CREATE OR REPLACE PROCEDURE get_fk_tables_info(table_name VARCHAR)
IS
    num_entries INTEGER;
    num_distinct_fk INTEGER;
    cur NUMBER;
    query_ VARCHAR(500);
    ret INTEGER;
    r_table_name VARCHAR(200);
    r_column_name VARCHAR(200);
BEGIN
    query_ := 'SELECT c_pk.table_name r_table_name, a.column_name r_column_name
        FROM all_cons_columns a
        JOIN all_constraints c ON c.r_owner = user
                                AND c.constraint_name = a.constraint_name
        JOIN all_constraints c_pk ON c_pk.owner = user
                                AND c_pk.constraint_name = c.r_constraint_name
        WHERE c.constraint_type = ''R'' AND a.table_name = :table_name';
    cur := dbms_sql.open_cursor;
    dbms_sql.parse(cur, query_, dbms_sql.v7);
    dbms_sql.bind_variable(cur, ':table_name', table_name);
    ret := dbms_sql.execute(cur);

    dbms_sql.define_column(cur, 1, r_table_name, 200);
    dbms_sql.define_column(cur, 2, r_column_name, 200);
    LOOP
        -- проходимся по дочерним таблицам
        EXIT WHEN dbms_sql.fetch_rows(cur) = 0;
        dbms_sql.column_value(cur, 1, r_table_name);
        dbms_sql.column_value(cur, 2, r_column_name);

        EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || r_table_name INTO num_entries;
        EXECUTE IMMEDIATE 'SELECT COUNT(DISTINCT ' || r_column_name || ') FROM ' 
            || table_name INTO num_distinct_fk;

        dbms_output.put_line(r_table_name || ': ' || num_entries || ' entries, ' 
                             || num_distinct_fk || ' distinct foreign keys');
    END LOOP;
    
    dbms_sql.close_cursor(cur);
EXCEPTION 
    WHEN OTHERS THEN 
        dbms_sql.close_cursor(cur);
        RAISE;
END;
/

-- Проверка:
BEGIN
    get_fk_tables_info('DISSERTATIONS');
END; 
/
