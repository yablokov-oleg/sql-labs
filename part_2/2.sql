-- 1:
-- Процедура, выдающая отчет по диссертациям, защищенным в определенном году:
-- Год
--  Раздел1
--      Направление1
--          ФИО1        дата защиты    название диссертации
--          ФИО2        дата защиты    название диссертации
-- …
--      Направление2
--          ФИО1        дата защиты    название диссертации
-- …
--  Раздел2
--      Направление1
--          ФИО1        дата защиты    название диссертации
-- …
-- Сначала для каждого научного направления выводятся данные о докторских диссертациях, затем – о кандидатских. 
-- Параметр – год защиты. Использовать функцию из лабораторной работы №1.
CREATE OR REPLACE PROCEDURE get_dissertations_report(year_to_show INTEGER)
IS
    dis_count INTEGER := 0;
BEGIN
    SELECT COUNT(*) 
    INTO dis_count 
    FROM dissertations dis 
    WHERE EXTRACT(YEAR FROM dis.date_defence) = year_to_show;
    
    IF (dis_count = 0) THEN 
        RAISE NO_DATA_FOUND;
    END IF;
    
    dbms_output.put_line(year_to_show);
    FOR section IN (SELECT * FROM science_sections)
    LOOP
        dbms_output.put_line(section.name);
        FOR area IN (
            SELECT * 
            FROM scientific_areas areas 
            WHERE areas.science_section_cipher = section.cipher
        )
        LOOP
            dbms_output.put_line('__' || area.name);
            FOR el IN (
                SELECT authors.full_name, dis.date_defence, dis.name
                FROM dissertations dis, authors
                WHERE (
                    dis.scientific_area = area.id AND
                    dis.author_passport_id = authors.passport_id AND
                    EXTRACT(YEAR FROM dis.date_defence) = year_to_show
                )
                ORDER BY dis.type
            )
            LOOP
                dbms_output.put_line('____' || RPAD(get_reduced_fio(el.full_name), 25) || RPAD(el.date_defence, 25) || RPAD(el.name, 25));
            END LOOP;
        END LOOP;
    END LOOP;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        raise_application_error(-20023, 'Не найдены диссертации за этот год');
END;
/

-- Проверка:
BEGIN
    get_dissertations_report(2016);
END; 
/



-- 2:
-- Процедура поиска авторов, которые внесены в таблицу "Авторы" дважды. Идентификация автора происходит по совпадению 
-- ФИО и даты рождения (паспортные данные могут измениться). Если при этом диссертации (кандидатская и докторская) защищены 
-- по одному направлению, то это один и тот же человек. Данные о нем объединяются: в таблице "Авторы" остается одна строка 
-- с более поздней датой выдачи паспорта.
CREATE OR REPLACE PROCEDURE fix_authors_duplications
IS
BEGIN
    FOR author1 IN (SELECT * FROM authors) LOOP
        FOR author2 IN (SELECT * FROM authors WHERE passport_id != author1.passport_id) LOOP
            IF (author1.full_name = author2.full_name) AND (author1.birth = author2.birth) THEN
                FOR el1 IN (
                    SELECT dis.type, sections.cipher FROM dissertations dis, scientific_areas areas, science_sections sections
                    WHERE (
                        dis.scientific_area = areas.id AND
                        areas.science_section_cipher = sections.cipher AND
                        dis.author_passport_id = author1.passport_id))
                LOOP
                    FOR el2 IN (
                        SELECT dis.type, sections.cipher FROM dissertations dis, scientific_areas areas, science_sections sections
                        WHERE (
                            dis.scientific_area = areas.id AND
                            areas.science_section_cipher = sections.cipher AND
                            dis.author_passport_id = author2.passport_id)) 
                    LOOP
                        IF (el1.type != el2.type) AND (el1.cipher = el2.cipher) THEN
                            IF author1.passport_date_receipt < author2.passport_date_receipt THEN
                                UPDATE dissertations
                                    SET author_passport_id = author2.passport_id
                                    WHERE author_passport_id = author1.passport_id;
                                DELETE FROM authors WHERE passport_id = author1.passport_id;
                                dbms_output.put_line(author1.full_name || ': обновление паспортных данных (' || author1.passport_id || ' -> ' || author2.passport_id || ')');
                            ELSE
                                UPDATE dissertations
                                    SET author_passport_id = author1.passport_id
                                    WHERE author_passport_id = author2.passport_id;
                                DELETE FROM authors WHERE passport_id = author2.passport_id;
                                dbms_output.put_line(author1.full_name || ': обновление паспортных данных (' || author2.passport_id || ' -> ' || author1.passport_id || ')');
                            END IF;
                        END IF;
                    END LOOP;
                END LOOP;
            END IF;
        END LOOP;
    END LOOP;
END;
/

-- Проверка:
BEGIN
    fix_authors_duplications();
END; 
/



-- 3:
-- Выдает список авторов с указанием ученой степени. Если автор защитил кандидатскую и докторскую диссертации по 
-- одному разделу, то он является доктором наук. Если разделы разные, то ученые степени перечисляются через запятую 
-- (например, 'кандидат экономических наук, доктор технических наук'). Использовать функцию из лабораторной работы №1.
CREATE OR REPLACE PROCEDURE get_authors_report
IS
    authors_count INTEGER := 0;
    scientific_degrees VARCHAR2(400);
    k_defenced INTEGER;
    d_defenced INTEGER;
BEGIN
    SELECT COUNT(*) 
    INTO authors_count 
    FROM authors;
    
    IF (authors_count = 0) THEN 
        RAISE NO_DATA_FOUND;
    END IF;
    
    FOR author in (SELECT * FROM authors)
    LOOP
        scientific_degrees := '';
        FOR section in (SELECT * FROM science_sections)
        LOOP
            k_defenced := 0;
            d_defenced := 0;
            FOR dis IN (
                SELECT dis.*
                FROM dissertations dis, scientific_areas areas
                WHERE (
                    dis.scientific_area = areas.id AND
                    areas.science_section_cipher = section.cipher AND
                    dis.author_passport_id = author.passport_id
                )
            )
            LOOP
                IF (dis.type = 'кандидатская') AND (dis.date_acceptance IS NOT NULL) THEN
                    k_defenced := 1;
                ELSIF (dis.type = 'докторская') AND (dis.date_acceptance IS NOT NULL) THEN
                    d_defenced := 1;
                END IF;
            END LOOP;
            
            IF (d_defenced = 1) THEN
                scientific_degrees := scientific_degrees || get_full_academic_degree('докторская', section.name) || ', ';
            ELSIF (k_defenced = 1) THEN
                scientific_degrees := scientific_degrees || get_full_academic_degree('кандидатская', section.name) || ', ';
            END IF;
        END LOOP;
        IF (scientific_degrees IS NULL) THEN
            scientific_degrees := '<без степени>';
        END IF;
        dbms_output.put_line(author.full_name || ': ' || scientific_degrees);
    END LOOP;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        raise_application_error(-20023, 'Не найдены авторы');
END;
/

-- Проверка:
BEGIN
    get_authors_report();
END; 
/
