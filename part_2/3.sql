-- 1:
-- Реализация ограничений внешнего ключа.
-- (реализовано для dissertations.author_passport_id)
CREATE OR REPLACE TRIGGER dissertations__passport_id_FK_constraint
BEFORE INSERT OR UPDATE ON dissertations
FOR EACH ROW
DECLARE
    n_count INTEGER;
BEGIN
    SELECT count(*) 
        INTO n_count 
        FROM authors 
        WHERE authors.passport_id = :new.author_passport_id;
        
    IF n_count = 0 THEN
        raise_application_error(-20101, 'Не найден автор с введёнными паспортными данными');
    END IF;
END;
/

CREATE OR REPLACE TRIGGER authors__passport_id_FK_constraint
BEFORE UPDATE OR DELETE ON authors
FOR EACH ROW
DECLARE
    n_count INTEGER;
BEGIN
    SELECT count(*) 
        INTO n_count 
        FROM dissertations
        WHERE dissertations.author_passport_id = :old.passport_id;
        
    IF n_count > 0 THEN
        raise_application_error(-20109, 'Невозможно изменить данные об авторе, на которого ссылаются диссертации');
    END IF;
END;
/



-- 2:
-- Проверка значений всех полей отношения "Диссертации", для которых могут быть определены домены 
-- (в т.ч., дата защиты и дата утверждения не могут быть больше текущей даты).
CREATE OR REPLACE TRIGGER dissertations__date_constraints
BEFORE INSERT OR UPDATE ON dissertations
FOR EACH ROW
DECLARE
BEGIN
    IF (:new.date_defence > sysdate) THEN
        raise_application_error(-20001, 'Дата защиты не может быть больше текущей даты');
    END IF;
    IF (:new.date_acceptance > sysdate) THEN
        raise_application_error(-20002, 'Дата утверждения не может быть больше текущей даты');
    END IF;
END;
/



-- 3:
-- Замена при добавлении данных сокращенных значений поля "Тип" отношения "Диссертации" 
-- на полные ("канд" на "кандидатская" и т.п.).
CREATE OR REPLACE TRIGGER dissertations__type_corrections
BEFORE INSERT OR UPDATE ON dissertations
FOR EACH ROW
DECLARE
BEGIN
    IF (:new.type = 'канд') OR (:new.type = 'к') THEN
        :new.type := 'кандидатская';
    END IF;
    IF (:new.type = 'док') OR (:new.type = 'д') THEN
        :new.type := 'докторская';
    END IF;
END;
/



-- 4:
-- Триггер, переносящий в архив (в специальную таблицу) изменения сведений об авторах.

-- Создадим вспомогательную таблицу (архив авторов):
CREATE TABLE authors_archive(
    full_name               VARCHAR(100) NOT NULL,
    birth                   DATE NOT NULL,
    sex                     CHAR(2) NOT NULL,
    passport_id             CHAR(10) PRIMARY KEY,
    passport_date_receipt   DATE NOT NULL,
    datetime_archived       DATE NOT NULL, -- Для поддержки версионности

    CHECK(sex = 'м' OR sex = 'ж')
);

-- Триггер:
CREATE OR REPLACE TRIGGER archive_authors
BEFORE UPDATE ON authors
FOR EACH ROW
DECLARE
BEGIN
    INSERT INTO authors_archive VALUES(:old.full_name, :old.birth, :old.sex, :old.passport_id, :old.passport_date_receipt, sysdate);
END;
/
